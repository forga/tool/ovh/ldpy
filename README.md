LDPy
====

This project moved to: [`git.afpy.org`](https://git.afpy.org/fcode/ldpy)

🚧 _work in progress, take a look on other branches if you are curious…_

---

An [OVH Log Data Platform](https://docs.ovh.com/fr/logs-data-platform/) python client.

[Log Data Platform](https://docs.ovh.com/fr/logs-data-platform/) is a log management platform based on [`opensearch`](https://opensearch.org/).


💡 Idea
-------

1. Access LDP stream
1. Query the logs
1. Trigger actions


🚀 Use quick-start
-----------------

Get the source & edit settings:
```bash
git clone git@git.afpy.org:fcode/LDPy.git && cd ldpy
cp client.sample.py client.py
${EDITOR} client.py
```

Set up environment & run help:
```bash
mkdir ~/.venvs && python3 -m venv ~/.venvs/ldpy
source ~/.venvs/ldpy/bin/activate
pip install -r requirements.txt
./ldpy.py -h
```

* Get stream mapping: `./ldpy.py --mapping`
* Get last `n` entries: `./ldpy.py --last`


🚧 Devel quick-start
--------------------

- Built with `Python 3.8`
- Code linting with [`flake8`](https://pypi.org/project/flake8/), [`pylint`](https://pypi.org/project/pylint), [`black`](https://pypi.org/project/black) & [`pydocstyle`](https://pypi.org/project/pydocstyle/).
- Install development tools:
    * `pip install -r requirements-dev.txt`
- A `Makefile` with tools : run `make help` to have a look
- Use the `pre-commit` git hook
    * `echo "make --no-print-directory --quiet lint" > .git/hooks/pre-commit`
    * `chmod u+x .git/hooks/pre-commit`


### 📌 Dependences


Details in [`requirements.txt`](requirements.txt) & [`requirements-dev.txt`](requirements-dev.txt)

- [`opensearch-py`](https://opensearch.org/docs/latest/clients/python/)


### ℹ️ Talk about this project

- [**AFPy 2022/09**](https://www.meetup.com/fr-FR/python-afpy-lyon/events/287980312/): [`ftalk/ldpy!1`](https://gitlab.com/ftalk/ldpy/-/merge_requests/1)
