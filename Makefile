VBIN=${VIRTUAL_ENV}/bin

help: # Print help on Makefile
	@grep '^[^.#]\+:\s\+.*#' Makefile | \
	sed "s/\(.\+\):\s*\(.*\) #\s*\(.*\)/`printf "\033[93m"`\1`printf "\033[0m"`	\3/" | \
	expand -t20

open_all: # Open all projects files
	${EDITOR} ${VBIN}/activate
	${EDITOR} .gitignore .gitlab-ci.yml client* ldpy.py Makefile pyproject.toml README.md requirements-dev.txt requirements.txt tests_*
	${EDITOR} .git/hooks/p*-commit

pre_commit: # Run the pre-commit hook
	.git/hooks/pre-commit

clean: # Remove files not tracked in source control
	rm -rf .pytest_cache htmlcov __pycache__
	find . -type f -name "*.orig" -delete
	find . -type f -name "*.pyc" -delete
	find . -type d -empty -delete

lint: # Lint code
	${VBIN}/black --quiet --check *.py && echo "✅ black" || echo "🚨 black"
	${VBIN}/pflake8 --config=pyproject.toml && echo "✅ pflake8"  || echo "🚨 pflake8"
	${VBIN}/pydocstyle && echo "✅ pydocstyle"  || echo "🚨 pydocstyle"
	${VBIN}/pylint --rcfile=pyproject.toml *.py && echo "✅ pylint"  || echo "🚨 pylint"

test: # Test code
	${VBIN}/pytest tests_*.py

test_pdb: # Test code and open debugger if fail
	${VBIN}/pytest --pdb tests_*.py
